package com.cafe.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CafeManagementApp {

    public static void main(String[] args) {
        SpringApplication.run(CafeManagementApp.class, args);
    }
}
