package com.cafe.management.config;

import com.cafe.management.data.entity.user.User;
import com.cafe.management.data.entity.user.UserRole;
import com.cafe.management.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@Configuration
public class DataInitializationConfig {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void initializationManagers() {
        if (userRepository.findByUsername("User") == null) {
            User user = new User();
            user.setFullName("User Manager");
            user.setUsername("User");
            user.setPassword(passwordEncoder.encode("Welcome555"));
            user.setRole(UserRole.MANAGER);
            userRepository.save(user);
        }
    }
}
