package com.cafe.management.controller;

import com.cafe.management.data.entity.table.Table;
import com.cafe.management.data.entity.product.Product;
import com.cafe.management.service.OrderService;
import com.cafe.management.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tables")
public class TableController {

    @Autowired
    private TableService tableService;

    @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity getAll() {
        return ResponseEntity
                .ok(tableService.getAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Table table) {
        return ResponseEntity
                .ok(tableService.save(table));
    }

    @PutMapping
    public ResponseEntity update(@Valid @RequestBody Table table) {
        return ResponseEntity
                .ok(tableService.update(table));
    }

    @PostMapping("{tableId}/{userId}")
    public ResponseEntity addUser(
            @PathVariable("tableId") Long tableId, @PathVariable("userId") Long userId
    ) {
        return ResponseEntity
                .ok(tableService.addUser(tableId, userId));
    }

    @PostMapping("{tableId}")
    public ResponseEntity addOrder(
            @Valid @RequestBody List<Product> products,
            @PathVariable("tableId") Long tableId
    ) {
        return ResponseEntity.ok(orderService.save(products, tableId));
    }

    @GetMapping("/assigned")
    public ResponseEntity getAssigned() {
        return ResponseEntity
                .ok(tableService.getAssigned());
    }
}
