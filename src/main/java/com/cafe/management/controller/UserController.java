package com.cafe.management.controller;

import com.cafe.management.data.entity.user.User;
import com.cafe.management.security.AuthorizedUserService;
import com.cafe.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizedUserService authorizedUserService;

    @GetMapping
    public ResponseEntity getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody User user) {
        return ResponseEntity.ok(userService.save(user));
    }

    @GetMapping("/auth")
    public ResponseEntity getAuth() {
        return ResponseEntity.ok(authorizedUserService.getAuthorizedUser());
    }

    @GetMapping("/waiters")
    public ResponseEntity getWaiters() {
        return ResponseEntity.ok(userService.getWaiters());
    }

}
