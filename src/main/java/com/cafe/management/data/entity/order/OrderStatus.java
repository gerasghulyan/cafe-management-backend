package com.cafe.management.data.entity.order;

public enum OrderStatus {
    OPEN, CANCELLED, CLOSED
}
