package com.cafe.management.data.entity.product;

public enum ProductOrderStatus {
    ACTIVE, CANCELLED
}
