package com.cafe.management.data.entity.user;

public enum UserRole {
    MANAGER, WAITER
}
