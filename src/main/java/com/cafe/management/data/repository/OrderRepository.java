package com.cafe.management.data.repository;

import com.cafe.management.data.entity.table.Table;
import com.cafe.management.data.entity.order.Order;
import com.cafe.management.data.entity.order.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findByTableAndStatus(Table table, OrderStatus status);
}
