package com.cafe.management.data.repository;

import com.cafe.management.data.entity.table.Table;
import com.cafe.management.data.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TableRepository extends JpaRepository<Table, Long> {

    List<Table> findByUser(User user);
}
