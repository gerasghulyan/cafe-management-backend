package com.cafe.management.data.repository;

import com.cafe.management.data.entity.user.User;
import com.cafe.management.data.entity.user.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    List<User> findByRole(UserRole role);
}
