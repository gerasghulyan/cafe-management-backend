package com.cafe.management.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StatelessLoginFilter extends AbstractAuthenticationProcessingFilter {

    private final TokenAuthenticationService tokenAuthenticationService;
    private final UserDetailsService userDetailsService;

    public StatelessLoginFilter(
            String urlMapping,
            TokenAuthenticationService tokenAuthenticationService,
            UserDetailsService userDetailsService, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(urlMapping));
        this.userDetailsService = userDetailsService;
        this.tokenAuthenticationService = tokenAuthenticationService;
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {

        final AuthenticationCredentials credentials = new ObjectMapper().readValue(request.getInputStream(),
                AuthenticationCredentials.class);
        final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(
                credentials.getUsername(), credentials.getPassword());
        UserAuthentication userAuthentication = null;
        Authentication authentication = getAuthenticationManager().authenticate(loginToken);
        if (authentication != null) {
            userAuthentication = new UserAuthentication((UserDetails) authentication.getPrincipal());
        }
        return userAuthentication;
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request, HttpServletResponse response,
            FilterChain chain, Authentication authentication)
            throws IOException, ServletException {
        UserAuthentication userAuthentication = (UserAuthentication) authentication;
        tokenAuthenticationService.addAuthentication(response, userAuthentication);
        super.successfulAuthentication(request, response, chain, userAuthentication);
    }
}
