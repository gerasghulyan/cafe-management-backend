package com.cafe.management.security;

import com.cafe.management.data.entity.user.User;
import com.cafe.management.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("UserDetailsServiceImp")
public class UserDetailsServiceImp implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String login) throws RuntimeException {
        User user = repository.findByUsername(login);
        if (user == null) {
            throw new RuntimeException("Invalid login or password");
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (user.isManager()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_MANAGER"));
        } else {
            authorities.add(new SimpleGrantedAuthority("ROLE_WAITER"));
        }
        return new SecureUser(user, authorities);
    }
}

