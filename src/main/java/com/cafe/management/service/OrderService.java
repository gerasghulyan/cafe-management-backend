package com.cafe.management.service;

import com.cafe.management.data.entity.table.Table;
import com.cafe.management.data.entity.order.Order;
import com.cafe.management.data.entity.order.OrderStatus;
import com.cafe.management.data.entity.product.Product;
import com.cafe.management.data.entity.product.ProductOrder;
import com.cafe.management.data.entity.product.ProductOrderStatus;
import com.cafe.management.data.repository.OrderRepository;
import com.cafe.management.data.repository.ProductOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TableService tableService;

    @Autowired
    private ProductOrderRepository productOrderRepository;

    public Order save(List<Product> products, Long tableId)
            throws RuntimeException {
        Table table = tableService.get(tableId);
        if (table == null) {
            throw new RuntimeException("Something went wrong");
        }
        if (products.size() == 0) {
            throw new RuntimeException("Something went wrong");
        }
        Order order = orderRepository.findByTableAndStatus(table, OrderStatus.OPEN);
        if (order != null) {
            throw new RuntimeException("Something went wrong");
        }
        order = new Order();
        order.setId(null);
        order.setTable(table);
        order.setStatus(OrderStatus.OPEN);
        orderRepository.save(order);
        for (Product p : products) {
            ProductOrder po = new ProductOrder();
            po.setAmount(p.getAmount());
            po.setOrder(order);
            po.setProduct(p);
            po.setStatus(ProductOrderStatus.ACTIVE);
            productOrderRepository.save(po);

        }
        order.setProducts(products);
        return order;
    }

    public List<Order> getAll() {
        return orderRepository.findAll();
    }
}
