package com.cafe.management.service;

import com.cafe.management.data.entity.product.Product;
import com.cafe.management.data.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public Product update(Product product) throws RuntimeException {
        if (product.getId() == null) {
            throw new RuntimeException("Something went wrong");
        }
        return productRepository.save(product);
    }

}
