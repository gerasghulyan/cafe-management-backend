package com.cafe.management.service;

import com.cafe.management.data.entity.table.Table;
import com.cafe.management.data.entity.user.User;
import com.cafe.management.data.repository.TableRepository;
import com.cafe.management.security.AuthorizedUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TableService {

    @Autowired
    private TableRepository tableRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizedUserService authorizedUserService;

    public Table get(Long id) {
        return tableRepository.findOne(id);
    }

    public List<Table> getAll() {
        return tableRepository.findAll();
    }

    public Table save(Table table) {
        return tableRepository.save(table);
    }

    public Table update(Table table) {
        if (table.getId() == null) {
            throw new RuntimeException("Something went wrong");
        }
        return tableRepository.save(table);
    }

    public Table addUser(Long tableId, Long userId) throws RuntimeException {
        Table table = tableRepository.findOne(tableId);
        if (table == null) {
            throw new RuntimeException("Table with given parameters not found");
        }
        User user = userService.get(userId);
        if (user == null) {
            throw new RuntimeException("User with given parameters not found");
        }
        table.setUser(user);
        return tableRepository.save(table);
    }

    public List<Table> getAssigned() {
        return tableRepository.findByUser(authorizedUserService.getAuthorizedUser());
    }
}
