package com.cafe.management.service;

import com.cafe.management.data.entity.user.User;
import com.cafe.management.data.entity.user.UserRole;
import com.cafe.management.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User get(Long id) {
        return userRepository.findOne(id);
    }

    public User save(User newUser) throws RuntimeException {
        if (userRepository.findByUsername(newUser.getUsername()) != null) {
            throw new RuntimeException("Something went wrong");
        }
        return userRepository.save(newUser);
    }

    public List<User> getWaiters() {
        return userRepository.findByRole(UserRole.WAITER);
    }
}
